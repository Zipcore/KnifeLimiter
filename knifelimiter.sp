#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Knife Limiter"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "zipcore.net"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

#include <sourcemod>
#include <sdktools>
#include <multicolors>

int g_iWepType[MAXPLAYERS + 1];
bool g_bBlockKnife[MAXPLAYERS + 1];

public void OnPluginStart()
{
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("item_equip", Event_ItemEquip);
	HookEvent("player_death", Event_Death);
}

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_bBlockKnife[client] = false;
	
	return Plugin_Continue;
}

public Action Event_ItemEquip(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_iWepType[client] = GetEventInt(event, "weptype");
}

public Action Event_Death(Handle event, const char[] name, bool dontBroadcast)
{
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	
	char sWeaponName[64];
	GetEventString(event, "weapon", sWeaponName, sizeof(sWeaponName));
	
	if(StrContains(sWeaponName, "knife") != -1 || StrContains(sWeaponName, "bayonet") != -1)
	{
		g_bBlockKnife[attacker] = true;
		CPrintToChat(attacker, "{darkred}Your knife has been disabled until end of this round.");
	}
	
	return Plugin_Continue;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(!g_bBlockKnife[client])
		return Plugin_Continue;
	
	if(g_iWepType[client] == 0)
		BlockWeapon(client, weapon, 0.2);
  	
  	return Plugin_Continue;
}

void BlockWeapon(int client, int weapon, float time)
{
	float fUnlockTime = GetGameTime() + time;
	
	SetEntPropFloat(client, Prop_Send, "m_flNextAttack", fUnlockTime);
	
	if(weapon <= 0)
		weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	
	if(weapon > 0)
		SetEntPropFloat(weapon, Prop_Send, "m_flNextPrimaryAttack", fUnlockTime);
}